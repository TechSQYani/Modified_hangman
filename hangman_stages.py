#Import libraries required for displaying the hangman
from sense_hat import SenseHat
import time
#import threading
#import socket

#Socket code for connecting to the other Pi

#HOST='192.168.1.1'
#PORT=49960

#def data_from_server(x):
#    chances=x
#    return chances

sense = SenseHat()

X = [170, 170, 170]  # White
O = [0, 0, 0] #Blank/Off

#Define hangman "pieces"
gallows = [
O, O, O, X, X, X, O, O,
O, O, O, O, O, X, O, O,
O, O, O, O, O, X, O, O, 
O, O, O, O, O, X, O, O, 
O, O, O, O, O, X, O, O,
O, O, O, O, O, X, O, O, 
O, O, O, O, O, X, O, O,
O, O, O, X, X, X, X, X
]

head = [
O, O, O, X, O, O, O, O,
O, O, X, O, X, O, O, O,
O, O, O, X, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O
]

torso = [
O, O, O, X, O, O, O, O,
O, O, X, O, X, O, O, O,
O, O, O, X, O, O, O, O,
O, O, O, X, O, O, O, O,
O, O, O, X, O, O, O, O,
O, O, O, X, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O
]

arm1 = [
O, O, O, X, O, O, O, O,
O, O, X, O, X, O, O, O,
O, O, O, X, O, O, O, O,
O, X, X, X, O, O, O, O,
O, X, O, X, O, O, O, O,
O, O, O, X, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O
]

arm2 = [
O, O, O, X, O, O, O, O,
O, O, X, O, X, O, O, O,
O, O, O, X, O, O, O, O,
O, X, X, X, X, X, O, O,
O, X, O, X, O, X, O, O,
O, O, O, X, O, O, O, O,
O, O, O, O, O, O, O, O,
O, O, O, O, O, O, O, O
]

leg1 = [
O, O, O, X, O, O, O, O,
O, O, X, O, X, O, O, O,
O, O, O, X, O, O, O, O,
O, X, X, X, X, X, O, O,
O, X, O, X, O, X, O, O,
O, O, O, X, O, O, O, O,
O, O, X, O, O, O, O, O,
O, O, X, O, O, O, O, O
]

leg2 = [
O, O, O, X, O, O, O, O,
O, O, X, O, X, O, O, O,
O, O, O, X, O, O, O, O,
O, X, X, X, X, X, O, O,
O, X, O, X, O, X, O, O,
O, O, O, X, O, O, O, O,
O, O, X, O, X, O, O, O,
O, O, X, O, X, O, O, O
]

#Place the hangman "Pieces" into a list

#body = [gallows, head, torso, arm1, arm2, leg1, leg2]
body = [gallows, head, torso, arm1, arm2, leg1, leg2]

#define the display_hangman function, which displays the pieces in the list above depending on the amount of remaining chances

def display_hangman(chances):
    part=body[chances]
    while(chances>0):
        sense.clear()
        break
    sense.set_pixels(part)
    time.sleep(1.5)
    return part

#ignore everything below this line, it's old and is there as a "Just in Case"

#if __name__="__main__":
#    while 1:
        

#Below here is old, defunct, testing code which we've kept but commented out for archival's sake

#Valid Stages are gallows, head, torso, arm1, arm2, leg1 & leg2.

#def display_hangman(chances):
#    if chances == "7":
#        sense.set_pixels(gallows)
#        time.sleep(2)
#        sense.show_message("Welcome to hangman!")
    

#    elif chances == "6":
#        sense.set_pixels(head)
#        time.sleep(2)
#        sense.show_message("You just lost your head! You can only afford 5 more mistakes!")
#        time.sleep(2)

#    elif chances == "5":
#        sense.set_pixels(torso)
#        time.sleep(2)
#        sense.show_message("You just lost your torso! You can only afford 4 more mistakes!")
#        time.sleep(2)

#    elif chances == "4":
#        sense.set_pixels(arm1)
#        time.sleep(2)
#        sense.show_message("You just lost an arm! You can only afford 3 more mistakes!")
#        time.sleep(2)
    
#    elif chances == "3":
#        sense.set_pixels(arm2)
#        time.sleep(2)
#        sense.show_message("You just lost your other arm! You can only afford 2 more mistakes!")
#        time.sleep(2)

#    elif chances == "2":
#        sense.set_pixels(leg1) 
#        time.sleep(2)
#        sense.show_message("You just lost your a leg!")

#    elif chances == "1":
#        sense.set_pixels(leg2) 
#        time.sleep(2)
#        sense.show_message("You just lost your last leg! YOU DIED")
#        time.sleep(1)
#        sense.show_message("GAME OVER    GAME OVER    GAME OVER")


#For testing purposes: Display each hangman state, with a two second delay between each.

#sense.set_pixels(gallows)
#time.sleep(20)
#sense.set_pixels(head)
#time.sleep(1)
#
#sense.set_pixels(torso)
#time.sleep(1)
#
#sense.set_pixels(arm1)
#time.sleep(1)

#sense.set_pixels(arm2)
#time.sleep(1)

#sense.set_pixels(leg1)
#time.sleep(1)
#
#sense.set_pixels(leg2)
#time.sleep(1)
#
#sense.show_message(" ")

#quit()
