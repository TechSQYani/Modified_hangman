**# Modified_hangman** 

_A project by Yani (@TechSQYani) and Joseph (@TechsquareJoseph)_

This project is a modification and compilation of two python scripts, written by Delightful Dunlin and  Eager Elk (found [here](https://www.codegrepper.com/code-examples/python/python+hangman)), for use with a Raspberry Pi Sense Hat's 8x8 LED matrix.

The end goal is to have a hangman program that uses two 8x8 LED matrices on two Raspberry Pi sense hats to display the user's input (Letters) on one Pi and the state of the hangman (Gallows, body parts) on the other. This will involve python scripting to use the displays, for communication between the pis over the network, as well as to take input from the user (via a joystick), using the raspberry pi Sense hat.
