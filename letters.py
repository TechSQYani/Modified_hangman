#!/usr/bin/env python3
# The user input part of the hangman game, Sets up a scrollable list of letters, for use as input for the hangman game

#import libraries
from sense_hat import SenseHat
import random
from signal import pause
import socket
import threading
import time

sense = SenseHat()
sense.low_light = True

# Letter list
alphabet = [
    "A", "B", "C", "D", "E", "F", "G", "H", "I", 
"J", "K", "L", "M", "N", "O","P", "Q", "R",
"S", "T", "U", "V", "W", "X", "Y", "Z"
]

#make function that is callable
def get_input():
   global alphabet
   x = 0
   letter = alphabet[x]

#shows a letter in the list this is why x=0 atm
   sense.show_letter(alphabet[x])
   event = sense.stick.wait_for_event()

	
#Set up while loop so that the player can move the joystick left and right to
#go through the list

   game=True
   while game==True:
      sense.show_letter(alphabet[x])
      for event in sense.stick.get_events():
         if event.direction == 'right':
            x+=1
            if x>25:
               x=0
               sense.show_letter(alphabet[x])
            else:
               sense.show_letter(alphabet[x])
#Next line makes sure to flush out extra events before the next input
               event = sense.stick.wait_for_event(emptybuffer=True)
         elif event.direction == 'left': 
            x-=1
            if x<0:
               x=25
               sense.show_letter(alphabet[x])
            else:
               sense.show_letter(alphabet[x])
               event = sense.stick.wait_for_event(emptybuffer=True)
#Press up to clear the screen and break the loop
         elif event.direction == 'up':
            selected = "END"
            sense.clear()
            quit()
            game=False;
#press on the joystick to select a letter. Make sure that it waits for new input
         elif event.direction == 'middle':
            selected=alphabet[x]
            sense.stick.get_events()
            #Comment out next line when not needed to clear screen
           # sense.clear()
            game=False
   return selected

HOST = input("Please enter host IP: ")
print("When the game is finished please press up with the joystick.")
#HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 49960        # The port used by the server

#Creates a client that connects to the Server to send a letter via sockets post selection
#with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s: 
#Have the program pause to wait for the server to scroll through the message
s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
while True:

   letter=get_input()
#   print(letter)
   let_en=letter.encode('utf-8')
   s.sendall(let_en)
   time.sleep(2)


