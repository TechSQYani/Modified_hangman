#!/usr/bin/env python3
#The main hangman game, Displays the hangman, remaining attempts, and a blank for each letter in the chosen word.

#import assets for hangman
#from letters import get_input
import random
from hangman_stages import display_hangman
from sense_hat import SenseHat
import time
import socket
import encodings

sense = SenseHat()
sense.low_light = True
#Import port and request Server IP
HOST=input("Enter the IP address ")
#HOST='127.0.0.1' #Loopback IP for local testing
PORT=49960

#extract random word from the text file
def selection(name):
    file = open ("hangmanlist.txt", "r+")
    random_word = random.choice(file.read().split())
    file.close()
    return random_word

random_word=selection("hangman_list.txt")

#Display random word
def word_selected_dashed():
    word_selected_dashed = []
    for i in range(len(random_word)):
        word_selected_dashed.append('_')
    return ''.join(word_selected_dashed)

#This defines the server that will open and close to send the letter to the client(Letters.py)
#It returns the data gained from letters.py as a guessed letter

#This defines a way to end the loop on the client side
def finish(word):
         conn.sendall(word.encode('utf-8'))
         s.close()
         quit()
#Opens server and sends the length of the word to the client
length=len(random_word)
#sets the network module as variable to facilitate use
s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen()
#set connection and address for sockets
conn, addr = s.accept()
s.setblocking(False)
#conn.sendall(str(length).encode('utf-8'))
#         s.close()
#         time.sleep(1)

word_selected_dashed = word_selected_dashed()
sense.show_message("Welcome to hangman!", 0.075)
sense.show_message(word_selected_dashed)

chances = 6
#comment this line out when not testing
print (random_word)
#set the guessed word into dashes
guessed_word = list(word_selected_dashed)
sense.show_message('You have '+ str(chances)+ ' chances. ',0.075, text_colour=[255,255,255])
sense.show_message('Pick a letter', 0.075)

while chances > 0:
       if ''.join(guessed_word) == random_word:
         sense.show_message("Congrats", 0.075, text_colour=[0,255,0])
         s.close()
         break
#         finish(mes)

       data=conn.recv(1024).decode('utf-8')
       user_guessed_letter = str(data.lower())

       if user_guessed_letter in random_word:
          sense.load_image("check.png")
          time.sleep(1.5)
          for i in range(length):
             if list(random_word)[i] == user_guessed_letter:
                guessed_word[i] = user_guessed_letter
          sense.show_message(''.join(guessed_word))
          time.sleep(1)

       elif user_guessed_letter not in random_word:
         sense.show_message('X!',0.075, text_colour=[255,0,0])
         chances -= 1
         hang=display_hangman(chances=(6-chances))
if chances == 0:
      sense.show_message('Game Over', 0.075, text_colour=[255,0,0])
      s.close()
#      finish(mes)
